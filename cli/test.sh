#!/bin/bash

docker-compose up -d
docker exec -it store-pro vendor/bin/phpunit --coverage-text --testsuite=unit
docker exec -it store-pro vendor/bin/phpunit --testsuite=functional
docker-compose down