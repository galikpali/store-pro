# Store Pro #

README of Store pro repository.

## How to run tests
1. There must be a running docker daemon on your machine.
    * The docker engine must be version 18.06.0 or higher in order to launch the project
2. Run test.sh under cli dir
    * The script will run the unit test suite then functional test suite
    * The script will output the coverage stats covered by the unit test suite
    * The functional tests will either output or test the needed scenarios defined in the project requirements