<?php

namespace StorePro;

use PHPUnit\Framework\TestCase;
use StorePro\Exception\StorageFullException;
use StorePro\Interfaces\ProductInterface;
use StorePro\Interfaces\StorageInterface;

class DepotTest extends TestCase
{
    private Depot $depot;

    private StorageInterface $storageStub;

    protected function setUp(): void
    {
        $this->storageStub = $this->createMock(StorageInterface::class);
        $this->depot = new Depot($this->storageStub);
    }

    /** @throws StorageFullException */
    public function testAddProduct(): void
    {
        $this->depot->capacity = 1;
        $this->storageStub
            ->method('getProducts')
            ->willReturn([]);

        $productStub = $this->createMock(ProductInterface::class);
        $this->depot->addProduct($productStub);

        $this->addToAssertionCount(1);
    }

    /** @throws StorageFullException */
    public function testAddProductWillThrowExceptionIfCapacityExceeded(): void
    {
        $this->depot->capacity = 1;
        $productStub = $this->createMock(ProductInterface::class);

        $this->storageStub
            ->method('getProducts')
            ->willReturn([$this->createMock(ProductInterface::class)]);

        $this->expectException(StorageFullException::class);

        $this->depot->addProduct($productStub);
    }

    public function testGetProductsReturnStorageGetProductsReturnValue(): void
    {
        $this->storageStub
            ->method('getProducts')
            ->willReturn(['foo']);

        $this->assertEquals(['foo'], $this->depot->getProducts());
    }

    /** @throws Exception\ArticleNotFoundInStorageException */
    public function testRemoveProductByArticleNumberCallsStorageRemoveFunction(): void
    {
        $this->storageStub
            ->expects($this->exactly(1))
            ->method('removeProductByArticleNumber');

        $this->storageStub
            ->method('removeProductByArticleNumber')
            ->with(
                $this->callback(function (int $articleNumber) {
                    return $articleNumber === 1;
                })
            );

        $this->depot->removeProductByArticleNumber(1);
    }
}
