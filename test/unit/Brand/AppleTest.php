<?php

namespace StorePro\Brand;

use PHPUnit\Framework\TestCase;

class AppleTest extends TestCase
{
    private Apple $brand;

    protected function setUp(): void
    {
        $this->brand = new Apple();
    }

    public function testGetName(): void
    {
        $this->assertEquals('Apple', $this->brand->getName());
    }

    public function testGetQualityRating(): void
    {
        $this->assertEquals(4.7, $this->brand->getQualityRating());
    }
}
