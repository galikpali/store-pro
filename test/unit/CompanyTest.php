<?php

namespace StorePro;

use PHPUnit\Framework\TestCase;
use StorePro\Exception\AllDepotsAreFullException;
use StorePro\Exception\StorageFullException;
use StorePro\Interfaces\ProductInterface;

class CompanyTest extends TestCase
{
    private Company $company;

    protected function setUp(): void
    {
        $this->company = new Company();
    }

    /** @throws AllDepotsAreFullException */
    public function testStoreProduct(): void
    {
        $depotStub = $this->createMock(Depot::class);

        $this->company->addDepot($depotStub);

        $productStub = $this->createMock(ProductInterface::class);

        $this->company->storeProduct($productStub);

        $this->addToAssertionCount(1);
    }

    /** @throws AllDepotsAreFullException */
    public function testStoreProductWhenFirstDepotIsFull(): void
    {
        $depotStub1 = $this->createMock(Depot::class);
        $depotStub2 = $this->createMock(Depot::class);

        $depotStub1
            ->method('addProduct')
            ->willThrowException(new StorageFullException());

        $this->company->addDepot($depotStub1);
        $this->company->addDepot($depotStub2);

        $productStub = $this->createMock(ProductInterface::class);

        $this->company->storeProduct($productStub);

        $this->addToAssertionCount(1);
    }

    /** @throws AllDepotsAreFullException */
    public function testStoreProductWhenAllDepotsAreFull(): void
    {
        $depotStub1 = $this->createMock(Depot::class);
        $depotStub1
            ->method('addProduct')
            ->willThrowException(new StorageFullException());

        $this->company->addDepot($depotStub1);

        $productStub = $this->createMock(ProductInterface::class);

        $this->expectException(AllDepotsAreFullException::class);

        $this->company->storeProduct($productStub);
    }
}
