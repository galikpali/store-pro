<?php

namespace StorePro\Storage;

use PHPUnit\Framework\TestCase;
use StorePro\Exception\ArticleNotFoundInStorageException;
use StorePro\Interfaces\ProductInterface;

class InMemoryStorageTest extends TestCase
{
    public function testGetProductsReturnsAddedProduct(): void
    {
        $storage = new InMemoryStorage();

        $productStub = $this->createMock(ProductInterface::class);
        $storage->addProduct($productStub);

        $this->assertEquals([$productStub], $storage->getProducts());
    }

    public function testGetProductsReturnAddedProducts(): void
    {
        $storage = new InMemoryStorage();

        $productStub1 = $this->createMock(ProductInterface::class);
        $productStub2 = $this->createMock(ProductInterface::class);
        $storage->addProduct($productStub1);
        $storage->addProduct($productStub2);

        $this->assertEquals(
            [
                $productStub1,
                $productStub2,
            ],
            $storage->getProducts()
        );
    }

    /** @throws \StorePro\Exception\ArticleNotFoundInStorageException */
    public function testProductListEmptyAfterSingleProductIsRemoved(): void
    {
        $storage = new InMemoryStorage();
        $productStub = $this->createMock(ProductInterface::class);
        $productStub
            ->method('getArticleNumber')
            ->willReturn(12);

        $storage->addProduct($productStub);

        $this->assertEquals([$productStub], $storage->getProducts());
        $storage->removeProductByArticleNumber(12);
        $this->assertEquals([], $storage->getProducts());
    }

    /** @throws \StorePro\Exception\ArticleNotFoundInStorageException */
    public function testRemoveProductByArticleNumberThrowsException(): void
    {
        $storage = new InMemoryStorage();

        $this->expectException(ArticleNotFoundInStorageException::class);
        $this->expectExceptionMessage('10');

        $storage->removeProductByArticleNumber(10);
    }
}
