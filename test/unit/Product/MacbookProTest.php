<?php

namespace StorePro\Product;

use PHPUnit\Framework\TestCase;
use StorePro\Brand\Apple;

class MacbookProTest extends TestCase
{
    private MacbookPro $product;

    protected function setUp(): void
    {
        $this->product = new MacbookPro();
    }

    public function testGetId(): void
    {
        $this->assertEquals(1, $this->product->getArticleNumber());
    }

    public function testGetName(): void
    {
        $this->assertEquals('Macbook Pro', $this->product->getName());
    }

    public function testGetPrice(): void
    {
        $this->assertEquals(2500, $this->product->getPrice());
    }

    public function testGetColor(): void
    {
        $this->assertEquals('metal grey', $this->product->getColor());
    }

    public function testGetBrand(): void
    {
        $this->assertInstanceOf(Apple::class, $this->product->getBrand());
        $this->assertEquals('Apple', $this->product->getBrand()->getName());
        $this->assertEquals(4.7, $this->product->getBrand()->getQualityRating());
    }

    public function testToArray(): void
    {
        $this->assertEquals(
            [
                'articleNumber' => 1,
                'name' => 'Macbook Pro',
                'price' => 2500.0,
                'brand' => [
                    'name' => 'Apple',
                    'qualityRating' => 4.7,
                ],
                'color' => 'metal grey',
            ],
            $this->product->toArray()
        );
    }
}
