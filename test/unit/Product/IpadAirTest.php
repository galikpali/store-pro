<?php

namespace StorePro\Product;

use PHPUnit\Framework\TestCase;
use StorePro\Brand\Apple;

class IpadAirTest extends TestCase
{
    private IpadAir $product;

    protected function setUp(): void
    {
        $this->product = new IpadAir();
    }

    public function testGetId(): void
    {
        $this->assertEquals(2, $this->product->getArticleNumber());
    }

    public function testGetName(): void
    {
        $this->assertEquals('Ipad Air', $this->product->getName());
    }

    public function testGetPrice(): void
    {
        $this->assertEquals(800, $this->product->getPrice());
    }

    public function testGetWeight(): void
    {
        $this->assertEquals(456, $this->product->getWeight());
    }

    public function testGetBrand(): void
    {
        $this->assertInstanceOf(Apple::class, $this->product->getBrand());
        $this->assertEquals('Apple', $this->product->getBrand()->getName());
        $this->assertEquals(4.7, $this->product->getBrand()->getQualityRating());
    }

    public function testToArray(): void
    {
        $this->assertEquals(
            [
                'articleNumber' => 2,
                'name' => 'Ipad Air',
                'price' => 800.0,
                'brand' => [
                    'name' => 'Apple',
                    'qualityRating' => 4.7,
                ],
                'weight' => 456,
            ],
            $this->product->toArray()
        );
    }
}
