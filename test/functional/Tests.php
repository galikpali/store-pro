<?php

namespace StorePro;

use PHPUnit\Framework\TestCase;
use StorePro\Exception\AllDepotsAreFullException;
use StorePro\Product\IpadAir;
use StorePro\Product\MacbookPro;
use StorePro\Storage\InMemoryStorage;

class Tests extends TestCase
{
    /** @throws Exception\StorageFullException */
    public function testDepotStoreAndListProducts(): void
    {
        $depot = $this->createDepot(3, 'Some random depot name', 'Some random address');
        $depot->addProduct(new MacbookPro());
        $depot->addProduct(new MacbookPro());
        $depot->addProduct(new IpadAir());

        echo "\n";
        $this->outputDepot($depot);
        echo "\n\n";

        $this->addToAssertionCount(1);
    }

    /** @throws Exception\AllDepotsAreFullException */
    public function testStoreWithNotEnoughStorageCapacity(): void
    {
        $company = new Company();
        $company->addDepot($this->createDepot(1, 'Some random depot name', 'Some random address'));

        $this->expectException(AllDepotsAreFullException::class);

        $company->storeProduct(new MacbookPro());
        $company->storeProduct(new MacbookPro());
    }

    /** @throws Exception\AllDepotsAreFullException */
    public function testListProductsFromMultipleProducts(): void
    {
        $company = new Company();
        $company->addDepot($this->createDepot(4, 'Some random depot name', 'Some random address'));

        $company->storeProduct(new MacbookPro());
        $company->storeProduct(new IpadAir());

        echo "\n";
        foreach ($company->getDepots() as $depot) {
            $this->outputDepot($depot);
            echo "\n\n";
        }
        echo "\n";

        $this->addToAssertionCount(1);
    }

    private function outputDepot(Depot $depot): void
    {
        echo "depot name: {$depot->name}\n";
        echo "depot address: {$depot->address}\n";
        echo "products: \n";

        foreach ($depot->getProducts() as $product) {
            print_r($product->toArray());
            echo "\n";
        }
    }

    private function createDepot(int $capacity, string $name, string $address): Depot
    {
        $depot = new Depot($this->createInMemoryStorage());
        $depot->capacity = $capacity;
        $depot->name = $name;
        $depot->address = $address;

        return $depot;
    }

    private function createInMemoryStorage(): InMemoryStorage
    {
        return new InMemoryStorage();
    }
}
