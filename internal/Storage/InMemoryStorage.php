<?php

namespace StorePro\Storage;

use StorePro\Exception\ArticleNotFoundInStorageException;
use StorePro\Interfaces\ProductInterface;
use StorePro\Interfaces\StorageInterface;

class InMemoryStorage implements StorageInterface
{
    /** @var ProductInterface[] */
    private array $storage = [];

    public function addProduct(ProductInterface $product): void
    {
        $this->storage[] = $product;
    }

    /** @return ProductInterface[] */
    public function getProducts(): array
    {
        return $this->storage;
    }

    /**
     * @param int $articleNumber
     * @throws ArticleNotFoundInStorageException
     */
    public function removeProductByArticleNumber(int $articleNumber): void
    {
        foreach ($this->storage as $index => $product) {
            if ($product->getArticleNumber() === $articleNumber) {
                unset($this->storage[$index]);
                return;
            }
        }

        throw new ArticleNotFoundInStorageException($articleNumber);
    }
}
