<?php

namespace StorePro;

use StorePro\Exception\StorageFullException;
use StorePro\Interfaces\ProductInterface;
use StorePro\Interfaces\StorageInterface;

class Depot
{
    public string $name;

    public string $address;

    public int $capacity;

    private StorageInterface $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param ProductInterface $product
     * @throws StorageFullException
     */
    public function addProduct(ProductInterface $product): void
    {
        if (count($this->storage->getProducts()) >= $this->capacity) {
            throw new StorageFullException();
        }

        $this->storage->addProduct($product);
    }

    /** @return ProductInterface[] */
    public function getProducts(): array
    {
        return $this->storage->getProducts();
    }

    /**
     * @param int $articleNumber
     * @throws Exception\ArticleNotFoundInStorageException
     */
    public function removeProductByArticleNumber(int $articleNumber): void
    {
        $this->storage->removeProductByArticleNumber($articleNumber);
    }
}
