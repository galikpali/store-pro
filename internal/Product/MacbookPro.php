<?php

namespace StorePro\Product;

use StorePro\Interfaces\ProductInterface;
use StorePro\Product\Traits\AppleBrandAwareTrait;
use StorePro\Product\Traits\BasicToArrayCapableTrait;

class MacbookPro implements ProductInterface
{
    use AppleBrandAwareTrait;
    use BasicToArrayCapableTrait {
        toArray as traitToArray;
    }

    public function getArticleNumber(): int
    {
        return 1;
    }

    public function getName(): string
    {
        return 'Macbook Pro';
    }

    public function getPrice(): float
    {
        return 2500;
    }

    public function getColor(): string
    {
        return 'metal grey';
    }

    public function toArray(): array
    {
        return array_merge(
            $this->traitToArray(),
            ['color' => $this->getColor()]
        );
    }
}
