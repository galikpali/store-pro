<?php

namespace StorePro\Product;

use StorePro\Interfaces\ProductInterface;
use StorePro\Product\Traits\AppleBrandAwareTrait;
use StorePro\Product\Traits\BasicToArrayCapableTrait;

class IpadAir implements ProductInterface
{
    use AppleBrandAwareTrait;
    use BasicToArrayCapableTrait {
        toArray as traitToArray;
    }

    public function getArticleNumber(): int
    {
        return 2;
    }

    public function getName(): string
    {
        return 'Ipad Air';
    }

    public function getPrice(): float
    {
        return 800;
    }

    public function getWeight(): int
    {
        return 456;
    }

    public function toArray(): array
    {
        return array_merge(
            $this->traitToArray(),
            ['weight' => $this->getWeight()]
        );
    }
}
