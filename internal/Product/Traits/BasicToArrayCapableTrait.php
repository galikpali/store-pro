<?php

namespace StorePro\Product\Traits;

trait BasicToArrayCapableTrait
{
    public function toArray(): array
    {
        return [
            'articleNumber' => $this->getArticleNumber(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'brand' => [
                'name' => $this->getBrand()->getName(),
                'qualityRating' => $this->getBrand()->getQualityRating(),
            ],
        ];
    }
}
