<?php

namespace StorePro\Product\Traits;

use StorePro\Brand\Apple as AppleBrand;
use StorePro\Interfaces\BrandInterface;

trait AppleBrandAwareTrait
{
    public function getBrand(): BrandInterface
    {
        return new AppleBrand();
    }
}
