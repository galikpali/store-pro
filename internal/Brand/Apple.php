<?php

namespace StorePro\Brand;

use StorePro\Interfaces\BrandInterface;

class Apple implements BrandInterface
{
    public function getName(): string
    {
        return 'Apple';
    }

    public function getQualityRating(): float
    {
        return 4.7;
    }
}
