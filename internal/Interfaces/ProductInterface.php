<?php

namespace StorePro\Interfaces;

interface ProductInterface
{
    public function getArticleNumber(): int;

    public function getName(): string;

    public function getPrice(): float;

    public function getBrand(): BrandInterface;

    public function toArray(): array;
}
