<?php

namespace StorePro\Interfaces;

interface BrandInterface
{
    public function getName(): string;

    public function getQualityRating(): float;
}
