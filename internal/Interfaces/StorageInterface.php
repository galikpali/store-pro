<?php

namespace StorePro\Interfaces;

use StorePro\Exception\ArticleNotFoundInStorageException;

interface StorageInterface
{
    public function addProduct(ProductInterface $product);

    /** @return ProductInterface[] */
    public function getProducts(): array;

    /**
     * @param int $articleNumber
     * @throws ArticleNotFoundInStorageException
     */
    public function removeProductByArticleNumber(int $articleNumber);
}
