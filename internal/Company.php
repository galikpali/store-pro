<?php

namespace StorePro;

use StorePro\Exception\AllDepotsAreFullException;
use StorePro\Exception\StorageFullException;
use StorePro\Interfaces\ProductInterface;

class Company
{
    /** @var Depot[] */
    private array $depots = [];

    public function addDepot(Depot $depot)
    {
        $this->depots[] = $depot;
    }

    /**
     * @codeCoverageIgnore
     * @return Depot[]
     */
    public function getDepots(): array
    {
        return $this->depots;
    }

    /**
     * @param ProductInterface $product
     * @throws AllDepotsAreFullException
     */
    public function storeProduct(ProductInterface $product): void
    {
        foreach ($this->depots as $depot) {
            try {
                $depot->addProduct($product);
                return;
            } catch (StorageFullException $e) {
                continue;
            }
        }

        throw new AllDepotsAreFullException();
    }
}
